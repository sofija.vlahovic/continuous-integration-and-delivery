import { ContinuousIntegrationDeliveryTemplatePage } from './app.po';

describe('ContinuousIntegrationDelivery App', function() {
  let page: ContinuousIntegrationDeliveryTemplatePage;

  beforeEach(() => {
    page = new ContinuousIntegrationDeliveryTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
