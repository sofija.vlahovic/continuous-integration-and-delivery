﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ContinuousIntegrationDelivery.Configuration;

namespace ContinuousIntegrationDelivery.Web.Host.Startup
{
    [DependsOn(
       typeof(ContinuousIntegrationDeliveryWebCoreModule))]
    public class ContinuousIntegrationDeliveryWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ContinuousIntegrationDeliveryWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ContinuousIntegrationDeliveryWebHostModule).GetAssembly());
        }
    }
}
