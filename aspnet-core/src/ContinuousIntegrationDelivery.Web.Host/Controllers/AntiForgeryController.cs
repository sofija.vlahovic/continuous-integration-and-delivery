using Microsoft.AspNetCore.Antiforgery;
using ContinuousIntegrationDelivery.Controllers;

namespace ContinuousIntegrationDelivery.Web.Host.Controllers
{
    public class AntiForgeryController : ContinuousIntegrationDeliveryControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
