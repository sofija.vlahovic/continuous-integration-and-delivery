using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ContinuousIntegrationDelivery.Controllers
{
    public abstract class ContinuousIntegrationDeliveryControllerBase: AbpController
    {
        protected ContinuousIntegrationDeliveryControllerBase()
        {
            LocalizationSourceName = ContinuousIntegrationDeliveryConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
