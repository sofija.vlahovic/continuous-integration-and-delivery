﻿using System.Collections.Generic;

namespace ContinuousIntegrationDelivery.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
