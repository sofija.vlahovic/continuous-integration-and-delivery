﻿using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.EntityFrameworkCore;
using ContinuousIntegrationDelivery.EntityFrameworkCore.Seed;

namespace ContinuousIntegrationDelivery.EntityFrameworkCore
{
    [DependsOn(
        typeof(ContinuousIntegrationDeliveryCoreModule), 
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class ContinuousIntegrationDeliveryEntityFrameworkModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<ContinuousIntegrationDeliveryDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        ContinuousIntegrationDeliveryDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        ContinuousIntegrationDeliveryDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ContinuousIntegrationDeliveryEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
