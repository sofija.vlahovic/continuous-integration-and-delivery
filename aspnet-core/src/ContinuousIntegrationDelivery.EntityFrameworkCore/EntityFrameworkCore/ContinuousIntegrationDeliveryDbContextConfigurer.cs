using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ContinuousIntegrationDelivery.EntityFrameworkCore
{
    public static class ContinuousIntegrationDeliveryDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ContinuousIntegrationDeliveryDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ContinuousIntegrationDeliveryDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
