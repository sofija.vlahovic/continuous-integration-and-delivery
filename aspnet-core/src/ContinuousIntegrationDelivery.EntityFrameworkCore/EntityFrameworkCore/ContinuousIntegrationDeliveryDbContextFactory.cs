﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ContinuousIntegrationDelivery.Configuration;
using ContinuousIntegrationDelivery.Web;

namespace ContinuousIntegrationDelivery.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ContinuousIntegrationDeliveryDbContextFactory : IDesignTimeDbContextFactory<ContinuousIntegrationDeliveryDbContext>
    {
        public ContinuousIntegrationDeliveryDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ContinuousIntegrationDeliveryDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ContinuousIntegrationDeliveryDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ContinuousIntegrationDeliveryConsts.ConnectionStringName));

            return new ContinuousIntegrationDeliveryDbContext(builder.Options);
        }
    }
}
