﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ContinuousIntegrationDelivery.Authorization.Roles;
using ContinuousIntegrationDelivery.Authorization.Users;
using ContinuousIntegrationDelivery.MultiTenancy;

namespace ContinuousIntegrationDelivery.EntityFrameworkCore
{
    public class ContinuousIntegrationDeliveryDbContext : AbpZeroDbContext<Tenant, Role, User, ContinuousIntegrationDeliveryDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public ContinuousIntegrationDeliveryDbContext(DbContextOptions<ContinuousIntegrationDeliveryDbContext> options)
            : base(options)
        {
        }
    }
}
