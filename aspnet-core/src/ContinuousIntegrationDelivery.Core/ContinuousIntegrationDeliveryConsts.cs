﻿namespace ContinuousIntegrationDelivery
{
    public class ContinuousIntegrationDeliveryConsts
    {
        public const string LocalizationSourceName = "ContinuousIntegrationDelivery";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
