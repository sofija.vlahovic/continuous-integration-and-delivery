﻿using Abp.Authorization;
using ContinuousIntegrationDelivery.Authorization.Roles;
using ContinuousIntegrationDelivery.Authorization.Users;

namespace ContinuousIntegrationDelivery.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
