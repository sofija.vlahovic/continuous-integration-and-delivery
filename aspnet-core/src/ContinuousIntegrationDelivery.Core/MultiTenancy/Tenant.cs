﻿using Abp.MultiTenancy;
using ContinuousIntegrationDelivery.Authorization.Users;

namespace ContinuousIntegrationDelivery.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
