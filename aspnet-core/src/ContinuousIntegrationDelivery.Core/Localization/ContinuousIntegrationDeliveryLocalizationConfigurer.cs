﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ContinuousIntegrationDelivery.Localization
{
    public static class ContinuousIntegrationDeliveryLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ContinuousIntegrationDeliveryConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ContinuousIntegrationDeliveryLocalizationConfigurer).GetAssembly(),
                        "ContinuousIntegrationDelivery.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
