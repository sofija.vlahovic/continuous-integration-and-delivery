using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ContinuousIntegrationDelivery.Configuration;
using ContinuousIntegrationDelivery.EntityFrameworkCore;
using ContinuousIntegrationDelivery.Migrator.DependencyInjection;

namespace ContinuousIntegrationDelivery.Migrator
{
    [DependsOn(typeof(ContinuousIntegrationDeliveryEntityFrameworkModule))]
    public class ContinuousIntegrationDeliveryMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ContinuousIntegrationDeliveryMigratorModule(ContinuousIntegrationDeliveryEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(ContinuousIntegrationDeliveryMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                ContinuousIntegrationDeliveryConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ContinuousIntegrationDeliveryMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
