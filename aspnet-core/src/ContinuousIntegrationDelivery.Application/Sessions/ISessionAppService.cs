﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ContinuousIntegrationDelivery.Sessions.Dto;

namespace ContinuousIntegrationDelivery.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
