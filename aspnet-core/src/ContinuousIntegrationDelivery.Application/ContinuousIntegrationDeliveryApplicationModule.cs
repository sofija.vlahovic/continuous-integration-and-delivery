﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ContinuousIntegrationDelivery.Authorization;

namespace ContinuousIntegrationDelivery
{
    [DependsOn(
        typeof(ContinuousIntegrationDeliveryCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ContinuousIntegrationDeliveryApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ContinuousIntegrationDeliveryAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ContinuousIntegrationDeliveryApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
