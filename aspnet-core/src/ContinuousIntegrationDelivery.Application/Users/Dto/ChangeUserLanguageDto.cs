using System.ComponentModel.DataAnnotations;

namespace ContinuousIntegrationDelivery.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}