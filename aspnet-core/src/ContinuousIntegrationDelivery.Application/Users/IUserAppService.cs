using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ContinuousIntegrationDelivery.Roles.Dto;
using ContinuousIntegrationDelivery.Users.Dto;

namespace ContinuousIntegrationDelivery.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
