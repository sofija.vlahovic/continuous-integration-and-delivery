﻿using System.Threading.Tasks;
using ContinuousIntegrationDelivery.Configuration.Dto;

namespace ContinuousIntegrationDelivery.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
