﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ContinuousIntegrationDelivery.Configuration.Dto;

namespace ContinuousIntegrationDelivery.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ContinuousIntegrationDeliveryAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
