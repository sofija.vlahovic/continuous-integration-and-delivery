﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ContinuousIntegrationDelivery.MultiTenancy.Dto;

namespace ContinuousIntegrationDelivery.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

